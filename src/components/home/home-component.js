import React, {Component} from "react";
import {connect} from "react-redux";


import {PostComponent} from "../post/post-component";
import {bindActionCreators} from "redux";
import {addPost} from "../../store/actions/postActions";
import {Alert} from "../../pure-component/alert";

class HomeComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            content: "",
            isAlertShown: false,
            isFormDisabled: false,
        }
    }

    componentWillMount() {
        //-- call API server, get all posts
    }

    render() {
        return (
            <div>
                <div>Welcome {this.props.userInfo.username}</div>
                <div>What are you thinking</div>
                <fieldset disabled={this.state.isFormDisabled}>
                    <textarea onChange={(event) => {
                        this.setState({content: event.target.value});
                    }}/>
                    <div>
                        <button className={"btn btn-primary"} onClick={() => {
                            this.setState({isFormDisabled: true});

                            this.props.addPost(this.props.token, this.props.userInfo.username, this.props.userInfo.userId, this.state.content)
                                .then(() => {
                                    this.setState({isAlertShown: true, isFormDisabled: false});
                                })
                                .catch(() => {

                                })
                        }}>Post
                        </button>
                    </div>
                </fieldset>

                <div>
                    {this.props.posts.map((post, index) => (
                        <PostComponent key={index} username={post.username} content={post.content}/>))}
                </div>

                {
                    this.state.isAlertShown &&
                    <Alert title={"Success"} message={"Add post success"} onOK={() => {
                        this.setState({isAlertShown: false});
                    }
                    }/>
                }
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        token: store.authReducers.token,
        userInfo: {
            userId: store.authReducers.userId,
            username: store.authReducers.username,
            loginToken: store.authReducers.token,
        },
        posts: store.postReducers.posts
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addPost
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);