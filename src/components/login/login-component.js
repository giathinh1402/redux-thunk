import React, {Component} from "react";

import {connect} from "react-redux";
import {Link, withRouter} from "react-router-dom";

import "./login.css";
import {loginUser} from "../../store/actions/authActions";
import {bindActionCreators} from "redux";
import {Alert} from "../../pure-component/alert";


class LoginComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",

            isAlertShown: false,
            login: false,
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.token) {
            this.props.history.push("/home");
        } else {
            this.setState({isAlertShown: true});
        }
    }

    login() {
        //--
        this.props.loginUser(this.state.username, this.state.password);
        // if (this.props.token !== "") {
        //     this.setState({login: true});
        //     this.props.history.push("/home");
        // }
    }

    render() {
        return (
            <div className={"d-flex flex-column align-items-center"}>
                <div>Username</div>
                <input type="text" onChange={(event) => {
                    this.setState({username: event.target.value});
                }}/>
                <div>Password</div>
                <input type="password" onChange={(event) => {
                    this.setState({password: event.target.value});
                }}/>
                <button className={"btn btn-primary"} type="button"
                        disabled={!this.state.username || !this.state.password}
                        onClick={() => {
                            this.login();
                        }}
                >Login
                </button>
                <Link to={"/register"}>Register account</Link>
                {this.state.isAlertShown &&
                <Alert title={"Error"} message={"Login failed"}
                       onOK={() => {
                           this.setState({isAlertShown: false});
                       }}/>
                }
                <div>{this.props.token}</div>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        token: store.authReducers.token,
        lastAction: store.authReducers.lastAction,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({loginUser}, dispatch);
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginComponent));