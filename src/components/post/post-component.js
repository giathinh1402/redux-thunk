import React, {Component} from "react";

class PostComponent extends Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <span>{this.props.username}</span>
                <span>{this.props.content}</span>
            </div>
        );
    }

}

export {PostComponent};