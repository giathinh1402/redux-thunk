import React, {Component} from "react";
import {Form, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

class RegisterComponent extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className={"d-flex flex-column align-items-center"}>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="email" placeholder=""/>
                        <Form.Text className="text-muted">
                            Only contains number and alphabetical character.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder=""/>
                        <Form.Text className="text-muted">
                            Please choose a strong password.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Repeat password</Form.Label>
                        <Form.Control type="password" placeholder=""/>
                        <Form.Text className="text-muted">

                        </Form.Text>
                    </Form.Group>
                </Form>

                <Button variant="primary">Register</Button>
                <Link to={"/"}>Go to login page</Link>
            </div>
        )
    }

}

export {RegisterComponent};