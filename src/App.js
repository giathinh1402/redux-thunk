import React, {Component} from 'react';
import {connect} from "react-redux";
import {Route, withRouter} from "react-router-dom";
import LoginComponent from "./components/login/login-component";
import {RegisterComponent} from "./components/register/register-component";
import HomeComponent from "./components/home/home-component";
import {bindActionCreators} from "redux";
import {loadStorage} from "./store/actions/storageAction";

import './App.css';
class App extends Component {

    constructor(props) {
        super(props);

    }

    componentWillMount() {
        this.props.loadStorage();
    }

    render() {
        return (
            <div>
                <Route path={"/"} exact={true} component={LoginComponent}/>
                <Route path={"/register"} component={RegisterComponent}/>
                <Route path={"/home"} component={HomeComponent}/>
            </div>
        )
    }

}

const mapDispatchToProps = (dispatch)=>{
  return bindActionCreators({
      loadStorage,
  }, dispatch)
};

export default withRouter(connect(undefined, mapDispatchToProps)(App));
