import axios from "axios";

const addPost = (token, username, userId, content) => {

    return (dispatch) => {

        return new Promise((resolve, reject)=>{
            dispatch({
                type: "POST_PROGRESS",
            });

            //-- call api
            axios.post("http://localhost:1992/admin/post", {content}, {headers: {"Authorization": token}})
                .then((response) => {
                    setTimeout(() => {
                        if (response.data.success) {
                            dispatch({
                                type: "ADD",
                                data: {
                                    username: username,
                                    content: content,
                                }
                            });
                            resolve();
                        } else {
                            dispatch({
                                type: "ADD_FAILED",
                            });
                            reject();
                        }
                    }, 3000);
                })
                .catch((err) => {
                    dispatch({
                        type: "OPERATION_FAILED",
                    });
                    reject();
                })
        });
    }

};

export {addPost};
