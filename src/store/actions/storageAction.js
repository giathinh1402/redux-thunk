import {STORAGE_LOAD_SUCCESS} from "../types";

const loadStorage = () =>{

    return (dispatch)=>{
        localStorage.getItem("MY_TOKEN", (err, data)=>{
           dispatch({
               type: STORAGE_LOAD_SUCCESS,
               data: JSON.parse(data),
           })
        });
    }

};

export {loadStorage};