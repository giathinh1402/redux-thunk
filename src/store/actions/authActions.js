import axios from "axios";

const loginUser = (username, password) => {

    return (dispatch) => {
        dispatch({
            type: "LOGIN_PROGRESS",
        });

        axios.post("http://localhost:1992/auth/login", {username, password})
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: "LOGIN_SUCCESS",
                        data: {userId: response.data.result.userId, username: username, token: response.data.result.token}
                    });
                } else {
                    dispatch({
                        type: "LOGIN_FAILED"
                    });
                }
            })
            .catch((err) => {
                dispatch({
                    type: "OPERATION_FAILED"
                });
            });
    };

};

export {loginUser};