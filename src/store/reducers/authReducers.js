const initialState = {
    userId: "",
    username: "",
    token: "",
    lastAction: "",
};

const authReducers = (state = initialState, action) => {

    console.log("auth reducer", action);

    state = {...state, lastAction: action.type};

    switch (action.type) {
        case "STORAGE_LOAD_SUCCESS":
            return {
                ...state,
                token: action.data
            };
        case "LOGIN_SUCCESS":
            return {
                ...state,
                userId: action.data.userId,
                username: action.data.username,
                token: action.data.token
            };
        default:
            return state;
    }

};

export {authReducers};