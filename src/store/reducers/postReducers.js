const initialState = {
    posts: [],
    lastAction: "",
};

const postReducers = (state = initialState, action) => {

    state = {...state, lastAction: action.type};

    switch (action.type) {
        case "ADD":
            return {
                ...state,
                posts: [
                    ...state.posts,
                    {username: action.data.username, content: action.data.content},
                ]
            };

        default:
            return state;
    }
};

export {postReducers};