import {applyMiddleware, combineReducers, createStore} from "redux";

import thunk from "redux-thunk";

import {postReducers} from "./reducers/postReducers";
import {authReducers} from "./reducers/authReducers";

const configStore = () => {
    return createStore(
        combineReducers({
            postReducers: postReducers,
            authReducers: authReducers,
        }),
        applyMiddleware(thunk)
    );
};

export {configStore};

