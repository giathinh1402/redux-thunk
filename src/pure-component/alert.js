import React, {PureComponent} from "react";

class Alert extends PureComponent {

    render() {
        return (
            <div className={"my-alert"}>
                <div>{this.props.title}</div>
                <div>{this.props.message}</div>
                <div>
                    {this.props.onCancel && <button className={"btn btn-secondary"}>Cancel</button>}
                    <button className={"btn btn-secondary"} onClick={() => {
                        this.props.onOK && this.props.onOK();
                    }}>OK
                    </button>
                </div>
            </div>
        );
    }
}

export {Alert};